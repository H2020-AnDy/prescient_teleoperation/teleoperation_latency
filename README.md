# Teleoperation Latency

Teleoperation with latency in the network. This is the code used for the experiments in the paper: [arxiv](https://arxiv.org/abs/2107.01281)

## Software requirements
*Please select the ""latency" branch when available*

On Ubuntu 16.04 or 18.04 (install everything in the docker_OpenSoT):
* [docker_OpenSoT](https://gitlab.inria.fr/locolearn/docker_OpenSoT)
* [YARP](https://www.yarp.it/latest/) 
* [teleoperation modules](https://gitlab.inria.fr/H2020-AnDy/prescient_teleoperation/teleoperation_modules)
* [promp](https://gitlab.inria.fr/H2020-AnDy/prescient_teleoperation/promp)
* [teleoperation controller](https://gitlab.inria.fr/H2020-AnDy/prescient_teleoperation/teleoperation/-/tree/latency)

On Windows (xsens + VR headset):
* [YARP](https://www.yarp.it/latest/)
* [Xsens-yarp streaming module](https://gitlab.inria.fr/H2020-AnDy/sensors.git)
* MVN Xsens
* Oculus App
* [Virtual Desktop](https://www.vrdesktop.net/)
* [Kinovea](https://www.kinovea.org/)
* Intel RealSense App (or other camera streamers according to the camera you are using)

## Hardware requirements

* Xsens MVN Link (motion capture-suit)
* Oculus VR headset
* Intel RealSense external camera (or any other camera)
* iCub robot

## Setup

The human operator has to wear the Xsens MVN Link suit and the Oculus VR headset.
The robot iCub has to be turned on and initialized with the N-pose.
The cameras of the robot and the external camera have to be connected to the screen of a computer running the Kinovea app which will delay the streaming on the Oculus through the application Virtual Desktop (we will see how later).

## Setting up the communication among modules

- Run a yarp server if one is not already running.
```
yarpserver
```

- If there is already a server running (for example that on the robot), check the ip and the namespace of the server (*yarpwhere* in the machine where the server is running)

- set the proper yarp namespace in both your Ubuntu and Windows machines
```
yarp namespace <namespace-server>
```

- and overwrite the yarp conf file
```
yarp conf <ip-address-server> 10000
```

## Preparing your modules

- Be sure you have installed everything and you are currently on the *latency* branch of both [teleoperation modules](https://gitlab.inria.fr/locolearn/teleoperation_modules/-/tree/latency), [promp](https://gitlab.inria.fr/H2020-AnDy/promp/-/tree/latency) and [teleoperation controller](https://gitlab.inria.fr/locolearn/teleoperation/-/tree/latency)

- Before launching the modules check their configuration files (https://gitlab.inria.fr/locolearn/teleoperation_modules/-/tree/latency/etc): retargeting_icub.ini and prompPred.ini

## Running an experiment

### On the Robot's computer (Ubuntu):

- Run the yarp GUI
```
yarpmanager
```
- Run the robot application 

- Run the robot cameras and connect all the associated sub-modules

### On your Ubuntu machine: 

- Run the yarp node for the teleoperation
```
yarprun --server /teleop
```

- Run the yarp GUI
```
yarpmanager
```

- Open folder *.../teleoperation-modules/etc/scripts*

- Run the modules *retargeting* and *promp_predictor*

- The modules will now wait to be connected to the yarp ports they are supposed to read from

### On your Windows machine:

#### Vision

- Visualize the robot camera images in a small window
```
yarpview --from /iCubNancy01/view/camera/left --w 120 --h 80
```

- Open the Intel RealSense application (or any equivalent camera streamer)

- Open the Kinovea program and select the streaming delay you want to apply
Now you are delaying the streaming from your screen, which has both the images from yarpview and Intel RealSense.

- Open the Oculus program and run the Virtual Desktop app
Now you should see the Kinovea window in your Oculus.

#### Motion capture

- Open the MVN Studio program and connect your suit

- Launch the [Xsens-yarp streaming module](https://gitlab.inria.fr/H2020-AnDy/sensors.git)

- The operator wearing the suit should now be ready to teleoperate the robot starting with the N-pose.

- Connect the Xsens streaming ports to your teleoperation modules running on Ubuntu:
```
yarp connect /xsens/JointAngles /retargeting/q:o
yarp connect /xsens/PoseQuaternion /retargeting/pos:o
yarp connect /xsens/COM /retargeting/com:o

yarp connect /xsens/JointAngles /prompred/xsens:q
yarp connect /xsens/PoseQuaternion /prompred/xsens:p
yarp connect /xsens/COM /prompred/xsens:c
```

### On your Ubuntu machine:

- Check the configuration file of your controller (https://gitlab.inria.fr/locolearn/teleoperation/-/tree/latency/conf): icub_conf.yaml

- Run the robot controller:
```
./opensot_main
```

- Be ready to stop the robot's controller with *Ctrl-C* in case anything does not go as expected.






 




